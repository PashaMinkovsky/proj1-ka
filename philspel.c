/*
 * Include the provided hashtable library
 */
#include "hashtable.h"

/*
 * Include the header file
 */
#include "philspel.h"

/*
 * Standard IO and file routines
 */
#include <stdio.h>

/*
 * General utility routines (including malloc())
 */
#include <stdlib.h>

/*
 * Character utility routines.
 */
#include <ctype.h>

/*
 * String utility routines
 */
#include <string.h>

/*
 * this hashtable stores the dictionary
 */
HashTable *dictionary;


int unitTest();

/*
 * the MAIN routine.  You can safely print debugging information
 * to standard error (stderr) and it will be ignored in the grading
 * process, in the same way which this does.
 */
int main(int argc, char **argv){
  if(argc != 2){
    fprintf(stderr, "Specify a dictionary\n");
    return 0;
  }
  /*
   * Allocate a hash table to store the dictionary
   */

  fprintf(stderr, "Creating hashtable\n");
  dictionary = createHashTable(2255, &stringHash, &stringEquals);

  fprintf(stderr, "%d\n", stringEquals("hello", "Hello"));

  fprintf(stderr, "Loading dictionary %s\n", argv[1]);
  readDictionary(argv[1]);
  fprintf(stderr, "Dictionary loaded\n");



  fprintf(stderr, "Processing stdin\n");
  processInput();

  /* main in C should always return 0 as a way of telling
     whatever program invoked this that everything went OK
     */
  return 0;
}


int unitTest() {
	return stringHash("Hello");
}


/*
 * You need to define this function. void *s can be safely casted
 * to a char * (null terminated string) which is done for you here for
 * convenience.
 */



unsigned int stringHash(void *s){
  char *string = (char *) s;
  int hashInt = 0;
  int stringLength = strlen(string);
  int i = 0;
  for (i = 0; i < stringLength; i++) {
	  hashInt = (127 * hashInt + string[i]) % 16908799;
  }
  return hashInt;
}

/*
 * You need to define this function.  It should return a nonzero
 * value if the two strings are identical (case sensitive comparison)
 * and 0 otherwise.
 */
int stringEquals(void *s1, void *s2){
  char *string1 = (char *) s1;
  char *string2 = (char *) s2;
  if (stringHash(string1) == stringHash(string2) && strlen(string1) == strlen(string2)) {
	  return 1;
  }
  return 0;
}

/*
 * this function should read in every word in the dictionary and
 * store it in the dictionary.  You should first open the file specified,
 * then read the words one at a time and insert them into the dictionary.
 * Once the file is read in completely, exit.  You will need to allocate
 * (using malloc()) space for each word.  As described in the specs, you
 * can initially assume that no word is longer than 60 characters.  However,
 * for the final 20% of your grade, you cannot assumed that words have a bounded
 * length You can NOT assume that the specified file exists.  If the file does
 * NOT exist, you should print some message to standard error and call exit(0)
 * to cleanly exit the program. Since the format is one word at a time, with
 * returns in between, you can
 * safely use fscanf() to read in the strings.
 */

//OG
void readDictionaryOG(char *filename){ //while its not a new line
  FILE *file = fopen(filename, "r");
  if (file) {
	  while (!feof(file)) {
		  char* actualWord = (char*) malloc(60 * sizeof(char));
		  fscanf(file, "%s", actualWord);
		  insertData(dictionary, actualWord, actualWord);
	  }
  } else {
	  fprintf(stderr, "Silly rabbit, this file doesn't exist!");
	  exit(0);
  }
}


//Case for more than 60 characters.
void readDictionary(char *filename){
  FILE *file = fopen(filename, "r");
  int mem = 60;
  int charCounter = 0;
  int i = 0;
  if (file) {
	  while (!feof(file)) {
		  char* actualWord = (char*) malloc(mem * sizeof(char));
		  char currentCharacter;
		  while ((currentCharacter = getc(file)) != '\n' && currentCharacter != EOF) {
			  charCounter++;
			  if (charCounter == (mem - 2)) {
				  mem = 2*mem;
				  actualWord = realloc(actualWord, mem*sizeof(char));
			  }
				  actualWord[i] = currentCharacter;
				  i++;
		  }
		  insertData(dictionary, actualWord, actualWord);
		  mem = 60;
		  charCounter = 0;
		  i = 0;
	  }
  } else {
	  fprintf(stderr, "Silly rabbit, this file doesn't exist!");
	  exit(0);
  }
}




/*
 * This should process standard input and copy it to standard output
 * as specified in specs.  EG, if a standard dictionary was used
 * and the string "this is a taest of  this-proGram" was given to
 * standard input, the output to standard output (stdout) should be
 * "this is a teast [sic] of  this-proGram".  All words should be checked
 * against the dictionary as they are input, again with all but the first
 * letter converted to lowercase, and finally with all letters converted
 * to lowercase.  Only if all 3 cases are not in the dictionary should it
 * be reported as not being found, by appending " [sic]" after the
 * error.
 *
 * Since we care about preserving whitespace, and pass on all non alphabet
 * characters untouched, and with all non alphabet characters acting as
 * word breaks, scanf() is probably insufficent (since it only considers
 * whitespace as breaking strings), so you will probably have
 * to get characters from standard input one at a time.
 *
 * As stated in the specs, you can initially assume that no word is longer than
 * 60 characters, but you may have strings of non-alphabetic characters (eg,
 * numbers, punctuation) which are longer than 60 characters. For the final 20%
 * of your grade, you can no longer assume words have a bounded length.
 */
void processInput(){
	char *word;
	char *secondWord;
	char *thirdWord;
	int characterCounter = 0;
	int i = 0;
	int mem = 60;
	while (!feof(stdin)) {
		word = (char *) malloc(60 * sizeof(char));
		secondWord = (char *) malloc(60 * sizeof(char));
		thirdWord = (char *) malloc(60 * sizeof(char));
		char currentCharacter = getchar();
		while (isalpha(currentCharacter)) {
			characterCounter++;
			if (characterCounter == (mem - 2)) { //ask about this
				int newMem = mem*2;
				mem = newMem;
				word  = realloc(word, newMem*sizeof(char)); //why isnt memory reallocation working?
				secondWord = realloc(secondWord, newMem*sizeof(char));
				thirdWord = realloc(thirdWord, newMem*sizeof(char));
			}
			word[i] = currentCharacter;
			secondWord[i] = currentCharacter;
			thirdWord[i] = currentCharacter;
			i++;
			currentCharacter = getchar();

		}
		word[i] = '\0';
		secondWord[i] = '\0';
		thirdWord[i] = '\0';
		char nonalpha = currentCharacter;
		int j;
		for (j = 1; j < sizeof(secondWord); j++) {
			secondWord[j] = tolower(secondWord[j]);
			thirdWord[j] = tolower(thirdWord[j]);
		}
		thirdWord[0] = tolower(thirdWord[0]);
		if (findData(dictionary, word)) {
			printf("%s", word);
		} else if (findData(dictionary, secondWord)) {
			printf("%s", word);
		} else if (findData(dictionary, thirdWord)) {
			printf("%s", word);
		} else if (strlen(word) > 0) {
			printf("%s", word);
			printf(" [sic]");
		}
		if (nonalpha != EOF) {
			printf ("%c", nonalpha);
		}
		i = 0;
		characterCounter = 0;
		free(word);
		free(secondWord);
		free(thirdWord);
	}


}

